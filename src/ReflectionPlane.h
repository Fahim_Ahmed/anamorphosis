#pragma once
#include <ofMain.h>

class ReflectionPlane {

public:
	ReflectionPlane();
	ReflectionPlane(string texPath);

	float theta = 180;
	int steps = 60;
	int verticalSteps = 20;

	float radiusMin = 50;
	float radiusMax = 200;

	float du;
	float dv;

	ofImage img;
	ofTexture tex;
	ofMesh mesh;
	
	int resX, resY;
	float cylRadius;
	float cylHeight;
	float posterW;
	float posterH;
	bool bWireframe;
	float rZ;
	int a = 255;
	int lineColor = 0x1bdd97;
	bool bHideGui = false;
	//int lineColor = 0xffd54f;

	bool bBlend = false;
	void calculateReflection();
	void draw();

private:
	ofTrueTypeFont font;
};