#pragma once

#include "ofMain.h"
#include "UIElement.h"

#define DOT_SIZE 1

class UIGrid : UIElement {
private:
	int offsetY, offsetX, sh, sw;
	ofFbo dotsFbo;

public:	
	UIGrid(int w = 560, int h = 438, int offsetX = 20, int offsetY = 20) : UIElement(UI_TYPE_GRID){
		this->sw = w;
		this->sh = h;
		this->offsetX = offsetX;
		this->offsetY = offsetY;

		dotsFbo.allocate(ofGetWidth(), ofGetHeight());
		drawGrid();
	}	

	void update (){
		
	}

	void draw(int x, int y){
		ofPushMatrix ();
		ofTranslate (x,y);
		
		ofSetColor(255);
		dotsFbo.draw(0,0);
	
		ofPopMatrix ();
	}

	void drawGrid(){
		int numDotsX = floor((float)sw/(float)offsetX);
		int numDotsY = floor((float)sh/(float)offsetY);

		//cout << numDotsX << " " << numDotsY << endl;

		int newSw = offsetX * numDotsX;
		int newSh = offsetY * numDotsY;

		dotsFbo.begin(true);
		for (int i = 0; i < numDotsY + 1; i++) {

			ofSetColor (COLOR_GRID_LINE);
			ofRect(0, i*offsetY, newSw, DOT_SIZE);

			for (int j = 0; j < numDotsX; j++){
				ofSetColor(COLOR_GRID_LINE);
				ofRect(j*offsetX, 0, DOT_SIZE, newSh);

				//ofSetColor(255);
				//ofRect(j*offsetX, i*offsetY, DOT_SIZE, DOT_SIZE);				
			}		
		}

		ofSetColor(COLOR_GRID_OUTLINER);
		ofRect(0, 0, newSw, DOT_SIZE);
		ofRect(0, 0, DOT_SIZE, newSh);
		ofRect(newSw, 0, DOT_SIZE, newSh);
		ofRect(0, newSh, newSw, DOT_SIZE);
		
		//separate loop for overlapping bug
		for (int i = 0; i < numDotsY + 1; i++) {
			for (int j = 0; j <= numDotsX; j++){
				ofSetColor(255,255,255,180);
				ofRect(j*offsetX, i*offsetY, DOT_SIZE, DOT_SIZE);				
			}		
		}
		
		dotsFbo.end();
	}

	int setWidth() const { return sw; }
	void getWidth(int val) { sw = val; }
	
	int setHeight() const { return sh; }
	void getHeight(int val) { sh = val; }

	int OffsetY() const { return offsetY; }
	void OffsetY(int val) { offsetY = val; }
	
	int OffsetX() const { return offsetX; }
	void OffsetX(int val) { offsetX = val; }
};