#pragma once

#include "ofMain.h"

class ScaleLine
{
private:
public:
	int x, y, w;

	ScaleLine() {}
	ScaleLine(int x, int y, int w)
	{
		this->x = x;
		this->y = y;
		this->w = w;
	}

	void update();
	
	void draw()
	{
		//ofSetLineWidth(2);
		// start vertical line
		ofDrawLine(x, y - 4, x, y + 4);

		// range line
		ofDrawLine(x, y, x+w, y);

		// end vertical line
		ofDrawLine(w + x, y - 4, w + x, y + 4);
		//ofSetLineWidth(1);
	}
};