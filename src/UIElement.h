#pragma once

#include "ofMain.h"

#define SIDEART_PATH "side.png"

#define UI_TYPE_LABEL 0
#define UI_TYPE_BUTTON 1
#define UI_TYPE_LABEL_VALUE_PAIR 2
#define UI_TYPE_GRID 3
#define UI_TYPE_POINT 4
#define UI_TYPE_SIDEART 5

#define BUTTON_WIDTH 150
#define BUTTON_HEIGHT 21
#define BUTTON_LABEL_WIDTH 95
#define BUTTON_LABEL_OFFSET 8
#define POINT_CIRCLE_RADIUS 6
#define POINT_CENTER_RADIUS 2

#define COLOR_DARK_GREY_BLUE 30, 33, 46
#define COLOR_GRAY_BLUE 67, 72, 89
#define COLOR_FONT_LIGHT 218, 226, 242
#define COLOR_FONT_DARK 112, 117, 140
#define COLOR_GRID_LINE 37, 40, 52, 130
#define COLOR_GRID_OUTLINER 48, 52, 66

class UIElement {

private:
	ofImage backgroundImage;
	int type;
	ofTrueTypeFont font;
	ofImage sideArt;
	string _label, _value;

public:	
	ofVec3f position;
	ofColor fontColor;

	UIElement(){};

	UIElement(int type){
		setup(type);
	};

	void setup(int type, ofColor col = ofColor(COLOR_FONT_LIGHT)){
		this->type = type;
		this->fontColor = col;
		
		int fontSize;		
		fontSize = type == 0 ? 14 : 19;

		if(type != UI_TYPE_POINT){
			ofTrueTypeFont::setGlobalDpi(72);
			font.loadFont("visitor2.ttf", fontSize, false, true);
			
		}

		position.x = 0;
		position.y = 0;
		position.z = 0;

		_label = "NULL";
		_value = "0";
	}

	void update() {
	}

	void draw(int x, int y) {
		int innerX, fontX, fontY;

		switch(type){
			case UI_TYPE_LABEL:				
				//ofSetColor(fontColor);
				font.drawString(_label, x, y);
			break;
			case UI_TYPE_BUTTON:
				//bg rect
				ofSetColor(COLOR_DARK_GREY_BLUE);				
				ofRect(x, y, BUTTON_WIDTH, BUTTON_HEIGHT);

				//inner rect
				innerX = x + BUTTON_WIDTH - BUTTON_LABEL_WIDTH - BUTTON_LABEL_OFFSET;
				ofSetColor(COLOR_GRAY_BLUE);
				ofRect(innerX, y, BUTTON_LABEL_WIDTH, BUTTON_HEIGHT);

				ofSetColor(218, 226, 242);
				fontX = innerX + BUTTON_LABEL_WIDTH * 0.5 - font.stringWidth(_label) * 0.5;
				fontY = y + BUTTON_HEIGHT * 0.5 + font.stringHeight(_label) * 0.5;
				font.drawString(_label, fontX, fontY);
			break;
			case UI_TYPE_LABEL_VALUE_PAIR:
				if(!font.isLoaded()){
					cout << "font not loaded" << endl;
					break;
				}

				//bg_rect
				ofSetColor(COLOR_DARK_GREY_BLUE);				
				ofRect(x, y, BUTTON_WIDTH, BUTTON_HEIGHT);

				//inner rect
				innerX = x + BUTTON_LABEL_OFFSET;
				ofSetColor(COLOR_GRAY_BLUE);
				ofRect(innerX, y, BUTTON_LABEL_WIDTH, BUTTON_HEIGHT);

				ofSetColor(COLOR_FONT_LIGHT);				
				fontX = innerX + BUTTON_LABEL_WIDTH * 0.5 - font.stringWidth(_label) * 0.5;
				fontY = y + BUTTON_HEIGHT * 0.5 + font.stringHeight(_label) * 0.5;
				font.drawString(_label, fontX, fontY);

				ofSetColor(COLOR_FONT_LIGHT);
				fontX = innerX + BUTTON_LABEL_WIDTH + (BUTTON_WIDTH - BUTTON_LABEL_WIDTH - BUTTON_LABEL_OFFSET) * 0.5 - font.stringWidth(_value) * 0.5;
				font.drawString(_value, fontX, fontY);
			break;
			case UI_TYPE_POINT:
				ofSetColor(255, 0, 50, 100);
				ofNoFill();
				ofPushMatrix();
				ofTranslate(x, y);
				ofRotate(45);
				ofRect(0  - POINT_CIRCLE_RADIUS, 0 - POINT_CIRCLE_RADIUS, POINT_CIRCLE_RADIUS*2, POINT_CIRCLE_RADIUS*2);
				ofPopMatrix();
				//ofCircle(x,y, POINT_CIRCLE_RADIUS);
				
				ofSetColor(255, 0, 50);
				ofFill();
				ofCircle(x,y, POINT_CENTER_RADIUS);
				//ofRect(x - 1, y - 1, POINT_CENTER_RADIUS, POINT_CENTER_RADIUS);
			break;
			case UI_TYPE_SIDEART:
				if(!sideArt.isAllocated()){
					cout << "image not loaded." << endl;
					break;
				}

				ofSetColor(255);
				ofPushMatrix();
				ofTranslate(x, y);
				sideArt.draw(0,0);
				ofPopMatrix();
				break;
		}
	}

	void loadImage(int type = -1){

		if(type = -1)
			type = this->type;

		switch (type) {
		case UI_TYPE_SIDEART:
			sideArt.loadImage(SIDEART_PATH);
			break;
		default:
			break;
		}
	}

	void setLabel(string label){ this->_label = label; }
	void setLabel(string label, int fontSize)
	{
		this->_label = label;
		this->font.loadFont("visitor2.ttf", fontSize, false, true);
	}
	void setValue(string val){ this->_value = val; }

	string getValue(){ return _value; }
	string getLabel(){ return _label; }


};