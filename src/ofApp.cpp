#include "ofApp.h"
#include <ofxOBJModel/src/ofxOBJGroup.h>
#include <ofxOBJModel/src/ofxOBJModel.h>

int m = 1;

//--------------------------------------------------------------
void ofApp::setup(){
	ofSetFrameRate(60);
	ofBackground(24, 28, 33);
	//ofBackground(31, 36, 43);

	ofSetWindowPosition(300, 50);
	ofEnableAntiAliasing();

	sw = ofGetWidth();
	sh = ofGetHeight();

	ofEnableNormalizedTexCoords();
	ofSetCircleResolution(72);
	ofTrueTypeFont::setGlobalDpi(72);
	font.loadFont("consola.ttf", 14);	

	cam.setDistance(400);
	
	overlay.loadImage("side.png");
	overlay.setImageType(OF_IMAGE_COLOR_ALPHA);

	rPlane = ReflectionPlane("owl.jpg");
	rPlane.bWireframe = false;
	
	rPlane2 = ReflectionPlane("1.jpg");
	rPlane2.bWireframe = false;
	rPlane2.rZ = 60;
	rPlane2.lineColor = 0xffd54f;
	
	ofxGuiSetFont("consola.ttf", 14);
	setupGui();

	demoTitle = UIElement(UI_TYPE_LABEL);
	demoTitle.setLabel("anamorphosis v0.03a", 19);

	imageSaveXL.init(3, 0, false);

	cout << "config loaded." << endl;
	cout << "anamorphosis v0.03a" << endl;
}

//--------------------------------------------------------------
void ofApp::update(){
	//calculateReflection();

	rPlane.cylRadius = cylRadius;
	rPlane.cylHeight = cylHeight;
	rPlane.posterW = posterW;
	rPlane.posterH = posterH;
	rPlane.resX= resX;
	rPlane.resY= resY;

	rPlane2.cylRadius = cylRadius;
	rPlane2.cylHeight = cylHeight;
	rPlane2.posterW = posterW2;
	rPlane2.posterH = posterH2;
	rPlane2.resX = resX2;
	rPlane2.resY = resY2;
	rPlane2.a = alphaVal;

	rPlane.calculateReflection();
	rPlane2.calculateReflection();

	line.x = -rPlane.cylRadius;
	line.y = 0;
	line.w = rPlane.cylRadius * 2;

	ofBackground(backColorR, backColorG, backColorB);
}

//--------------------------------------------------------------
void ofApp::draw(){	
	if (!bHideGui) {
		ofSetColor(255, 255, 255, 100);
		ofEnableAlphaBlending();
		overlay.draw(16, 172);
		ofDisableAlphaBlending();


		ofPushMatrix();
		ofTranslate(sw*0.5, sh*0.5);
		//ofScale(1.5, 1.5);

		// Draw cylinder base
		ofSetHexColor(0x8d97a6);
		ofDrawCircle(0, 0, cylRadius);

		//ofSetHexColor(0x1f242b);
		ofSetColor(backColorR, backColorG, backColorB);
		if (!bSaveImage) ofDrawCircle(0, 0, cylRadius - 6);

		ofPopMatrix();
	}

	imageSaveXL.begin();

	// Draw reflection
	if (bSaveImage) {
		
		ofBackground(0, 0);
		//ofScale(1, -1, 1);
		//ofTranslate(0, -sh, 0);
		
		rPlane.draw();
		rPlane2.draw();

		

	}
	else {
		
		rPlane.draw();
		rPlane2.draw();

		if (!bHideGui) {
			ofPushMatrix();
			ofTranslate(sw*0.5, sh*0.5);

			float cr = (cylRadius / ppiInput) * 2;

			ofSetHexColor(0x8d97a6);
			font.drawString("cylinder Diameter: " + ofToString(cr) + " inch", cylRadius + 15, -cylRadius - 15);

			ofTranslate(0, -cylRadius - 15);
			line.draw();

			ofTranslate(0, cylRadius + 15);

			ofSetHexColor(0x1bdd97);
			font.drawString("poster width: " + ofToString(posterW / ppiInput) + " inch", rPlane.radiusMax + 15, 20);
			font.drawString("poster 2 width: " + ofToString(posterW2 / ppiInput) + " inch", rPlane2.radiusMax + 15, 40);
			font.drawString("poster height: " + ofToString(cylHeight / ppiInput) + " inch", rPlane.radiusMax + 15, 60);

			ofPopMatrix();

			ofSetHexColor(0xa3aebf);
			demoTitle.draw(sw - 240, 72);


			gui.draw();
			gui2.draw();
		}
	}

	imageSaveXL.end();
}

void ofApp::setupGui() {
	float pw = 100;
	float theta = (pw / rPlane.radiusMin) * RAD_TO_DEG;
	float du = theta / rPlane.steps;

	gui.setup();
	gui.add(backColorR.set("Background", 24, 0, 255));
	gui.add(backColorG.set("Background", 28, 0, 255));
	gui.add(backColorB.set("Background", 33, 0, 255));
	
	gui.add(ppiInput.setup("Monitor PPI: ", 102.35));

	gui.add(cylRadius.set("cyl. radius", rPlane.radiusMin, 10, 400));
	gui.add(cylHeight.set("cyl. height", (rPlane.radiusMax - rPlane.radiusMin), 1, 1000));
	gui.add(posterW.set("poster width", pw, 10, 2 * PI * cylRadius));

	gui.add(resX.set("U", rPlane.steps, 18, (posterW / rPlane.radiusMin) * RAD_TO_DEG));
	gui.add(resY.set("V", rPlane.verticalSteps, 4, 50));

	gui.add(rZ.set("RotateZ", 0, 0, 360));

	cylRadius.addListener(this, &ofApp::onRadiusChange);
	posterW.addListener(this, &ofApp::onResXChange);
	rZ.addListener(this, &ofApp::onRz);

	gui2.setup();
	gui2.setPosition(8, 220);
	gui2.add(posterW2.set("poster 2 width", pw, 10, 2 * PI * cylRadius));

	gui2.add(resX2.set("U", rPlane2.steps, 18, (posterW2 / rPlane2.radiusMin) * RAD_TO_DEG));
	gui2.add(resY2.set("V", rPlane2.verticalSteps, 4, 50));

	gui2.add(rZ2.set("RotateZ", 0, 0, 360));
	gui2.add(alphaVal.set("Alpha", 0, 0, 255));

	posterW2.addListener(this, &ofApp::onResXChange2);
	rZ2.addListener(this, &ofApp::onRz2);
}

void ofApp::saveImage()
{
	bSaveImage = true;

	//update();
	//draw();

	string name = "MorphedImage/AM" + ofToString(ofGetUnixTime()) + ".png";
	imageSaveXL.finish(name, true);

//	imageSaver.allocate(sh, sh, OF_IMAGE_COLOR_ALPHA);
//
//	glReadBuffer(GL_COLOR_ATTACHMENT0_EXT);
//	glReadPixels(sw * 0.5 - sh * 0.5, 0, sh, sh, GL_RGBA, GL_UNSIGNED_BYTE, imageSaver.getPixels());
//
//	string name = "MorphedImage/AM" + ofToString(ofGetUnixTime()) + ".png";
//	imageSaver.saveImage(name, OF_IMAGE_QUALITY_BEST);

	bSaveImage = false;
}

void ofApp::onRz(float &value) {
	rPlane.rZ = value;
}

void ofApp::onResXChange(float &value) {
	float tmp = resX;
	resX.setMax((value / rPlane.radiusMin) * RAD_TO_DEG);
	resX = tmp;
}

void ofApp::onRadiusChange(float &value) {
	float tmp = rPlane.posterW;
	float pc = 2 * PI * rPlane.cylRadius;
	posterW.setMax(pc);
	posterW.set(tmp);

	tmp = rPlane2.posterW;
	pc = 2 * PI * rPlane2.cylRadius;
	posterW2.setMax(pc);
	posterW2.set(tmp);
}

void ofApp::onRz2(float &value) {
	rPlane2.rZ = value;
}

void ofApp::onResXChange2(float &value) {
	float tmp = resX2;
	resX2.setMax((value / rPlane2.radiusMin) * RAD_TO_DEG);
	resX2 = tmp;
}

void ofApp::exportMesh(ofMesh &mesh) {
//	ofxOBJGroup group;
//
//	for (float i = 0; i < steps; i++) {
//		for (float j = 0; j < verticalSteps - 1; j++) {
//			int a = i * verticalSteps + j;
//			int b = a + 1;
//			int c = i * verticalSteps + j + verticalSteps;
//			int d = c + 1;
//
//			int i0 = a;
//			int i1 = b;
//			int i2 = c;
//			int i3 = d;
//
//			ofxOBJFace face;
//			face.addVertex(mesh.getVertex(i0));
//			face.addVertex(mesh.getVertex(i1));
//			face.addVertex(mesh.getVertex(i2));
//
//			face.addNormal(mesh.getNormal(i0));
//			face.addNormal(mesh.getNormal(i1));
//			face.addNormal(mesh.getNormal(i2));
//
//			face.addTexCoord(mesh.getTexCoord(i0));
//			face.addTexCoord(mesh.getTexCoord(i1));
//			face.addTexCoord(mesh.getTexCoord(i2));
//
//			group.addFace(face);
//
//			ofxOBJFace face2;
//			face2.addVertex(mesh.getVertex(i1));
//			face2.addVertex(mesh.getVertex(i2));
//			face2.addVertex(mesh.getVertex(i3));
//
//			face2.addNormal(mesh.getNormal(i1));
//			face2.addNormal(mesh.getNormal(i2));
//			face2.addNormal(mesh.getNormal(i3));
//
//			face2.addTexCoord(mesh.getTexCoord(i1));
//			face2.addTexCoord(mesh.getTexCoord(i2));
//			face2.addTexCoord(mesh.getTexCoord(i3));
//
//			group.addFace(face2);
//		}
//	}
//
//	ofxOBJModel model;
//	model.addGroup(group);
//	model.save("mesh.obj");
	//model.save("mesh_" + ofToString(ofGetUnixTime()) + ".obj");
}

void ofApp::setNormals(ofMesh &mesh) {
	//The number of the vertices
	int nV = mesh.getNumVertices();

	//The number of the triangles
	int nT = mesh.getNumIndices() / 3;
	vector<ofPoint> norm(nV); //Array for the normals

	for (int t = 0; t<nT; t++) {
		//Get indices of the triangle t
		int i1 = mesh.getIndex(3 * t);
		int i2 = mesh.getIndex(3 * t + 1);
		int i3 = mesh.getIndex(3 * t + 2);
		
		//Get vertices of the triangle
		const ofPoint &v1 = mesh.getVertex(i1);
		const ofPoint &v2 = mesh.getVertex(i2);
		const ofPoint &v3 = mesh.getVertex(i3);
		
		//Compute the triangle's normal
		ofPoint dir = ((v2 - v1).crossed(v3 - v2)).normalized();
		
		//Accumulate it to norm array for i1, i2, i3
		norm[i1] += dir;
		norm[i2] += dir;
		norm[i3] += dir;
	}
	//Normalize the normal's length
	for (int i = 0; i<nV; i++) {
		norm[i].normalize();
	}
	//Set the normals to mesh
	mesh.clearNormals();
	mesh.addNormals(norm);
}

void ofApp::exit()
{
	cylRadius.removeListener(this, &ofApp::onRadiusChange);
	posterW.removeListener(this, &ofApp::onResXChange);
	rZ.removeListener(this, &ofApp::onRz2);

	posterW2.removeListener(this, &ofApp::onResXChange);
	rZ2.removeListener(this, &ofApp::onRz2);
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
	ofFileDialogResult openFileResult;

	switch(key)
	{
		case '1':
			openFileResult = ofSystemLoadDialog("Select a jpg or png");
			if (openFileResult.bSuccess) {
				m = 1;
				processOpenedFile(openFileResult);
			}
			else {
				//canceled
			}
			break;
		case '2':
			openFileResult = ofSystemLoadDialog("Select a jpg or png");

			if (openFileResult.bSuccess) {
				m = 2;
				processOpenedFile(openFileResult);
			}
			else {
				//canceled
			}
			break;
		case 'd':
			rPlane.bWireframe = !rPlane.bWireframe;
			rPlane2.bWireframe = !rPlane2.bWireframe;
			break;
		case 'b':
			rPlane2.bBlend = !rPlane2.bBlend;
			break;
		case 'p':
			saveImage();
			break;
		case 'f':
			ofToggleFullscreen();
			break;
		case 'h':
			bHideGui = !bHideGui;
			rPlane.bHideGui = bHideGui;
			rPlane2.bHideGui = bHideGui;
			break;
	}
}

void ofApp::processOpenedFile(ofFileDialogResult result) {
	ofFile file(result.getPath());

	if (file.exists()) {
		string fileExtension = ofToUpper(file.getExtension());

		if (fileExtension == "JPG" || fileExtension == "PNG") {
			if(m == 1) {
				rPlane.img.loadImage(result.getPath());
				rPlane.tex = rPlane.img.getTexture();
			}else {
				rPlane2.img.loadImage(result.getPath());
				rPlane2.tex = rPlane2.img.getTexture();
			}
			
		}
	}
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
	sw = w;
	sh = h;

	imageSaveXL.init(3, 0, false);
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
