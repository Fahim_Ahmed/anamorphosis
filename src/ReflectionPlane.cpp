#include "ReflectionPlane.h"

ReflectionPlane::ReflectionPlane(){}

ReflectionPlane::ReflectionPlane(string texPath)
{
	du = theta / steps;
	dv = (radiusMax - radiusMin) / verticalSteps;

	img.loadImage(texPath);
	tex = img.getTexture();

	mesh.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);

	ofTrueTypeFont::setGlobalDpi(72);
	font.loadFont("consola.ttf", 14);

	rZ = 0;
}

void ReflectionPlane::calculateReflection()
{
	mesh.clear();	

	steps = resX;
	verticalSteps = resY;

	radiusMin = cylRadius;
	radiusMax = radiusMin + cylHeight;

	theta = (posterW / radiusMin) * RAD_TO_DEG;

	du = theta / steps;
	dv = (radiusMax - radiusMin) / verticalSteps;

	// Vertices
	for (int i = 0; i <= steps; i++) {
		for (int j = 0; j < verticalSteps; j++) {
			float dt = DEG_TO_RAD * (i * du);

			float dx = (radiusMin + j*dv) * cos(dt);
			float dy = (radiusMin + j*dv) * sin(dt);

			ofVec3f v(dx, dy, 0);

			mesh.addVertex(v);
			mesh.addNormal(ofVec3f(0, 0, 1));
		}
	}

	// Indices
	for (float i = steps; i >= 0; i--) {
		for (float j = verticalSteps - 1; j >= 0; j--) {

			if (i != 0 && j != 0) {
				int ni = steps - i;
				int nj = verticalSteps - j - 1;

				int a = ni * verticalSteps + nj;
				int b = a + 1;
				int c = ni * verticalSteps + nj + verticalSteps;
				int d = c + 1;

				mesh.addIndex(a);
				mesh.addIndex(b);
				mesh.addIndex(c);

				mesh.addIndex(b);
				mesh.addIndex(c);
				mesh.addIndex(d);
			}

			// TexCoords
			float tx = i / steps;
			float ty = j / (verticalSteps - 1);

			mesh.addTexCoord(ofVec2f(tx, ty));
		}
	}
}

void ReflectionPlane::draw(){
	ofPushMatrix();
	ofTranslate(ofGetWidth() * 0.5, ofGetHeight() * 0.5);
	//ofScale(1.5, 1.5);

	ofRotateZ(rZ);
	ofEnableNormalizedTexCoords();
	ofEnableAlphaBlending();
	ofSetColor(255, 255, 255, a);

	if(bBlend) ofEnableBlendMode(OF_BLENDMODE_ADD);
	tex.bind();
	mesh.draw();
	tex.unbind();
	if(bBlend) ofDisableBlendMode();

	ofDisableAlphaBlending();

	if (bWireframe) {
		ofEnableAlphaBlending();
		ofSetColor(255, 255, 255, 15);
		mesh.drawWireframe();

		ofSetColor(255, 255, 255, 100);
		mesh.drawVertices();

		ofDisableAlphaBlending();
	}

	ofDisableNormalizedTexCoords();

	if (!bHideGui) {
		ofSetHexColor(lineColor);
		ofDrawLine(0, 0, radiusMax * 2, 0);
		ofRotateZ(theta);
		ofDrawLine(0, 0, radiusMax * 2, 0);
	}

	ofPopMatrix();

//	ofPushMatrix();
//	ofTranslate(ofGetWidth()*0.5, ofGetHeight()*0.5);
//	ofSetHexColor(0x1bdd97);
//
//	font.drawString("poster width: " + ofToString(posterW) + " unit", radiusMax * 1.5 + 15, 20);
//	font.drawString("poster height: " + ofToString(cylHeight) + " unit", radiusMax * 1.5 + 15, 40);
//
//	ofPopMatrix();
}