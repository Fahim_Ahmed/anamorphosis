#pragma once

#include "ofMain.h"
#include <ofxGui/src/ofxGui.h>
#include <ofxGui/src/ofxPanel.h>
#include "UIElement.h"
#include "ReflectionPlane.h"
#include <ofxInputField/src/ofxInputField.h>
#include "ScaleLine.h"
#include <ofxThreadedImage/src/ofxThreadedImage.h>
#include <ofxTileSaver/src/ofxTileSaver.h>

class ofApp : public ofBaseApp{
	private:
		ofImage overlay;

		UIElement demoTitle;

		ofEasyCam cam;

		ofxPanel gui;
		ofParameter<float> cylRadius;
		ofParameter<float> cylHeight;
		
		ofParameter<float> resX;
		ofParameter<float> resY;
		
		ofParameter<float> posterW;
		ofParameter<float> posterH;
		ofParameter<float> rZ;

		ofxPanel gui2;

		ofParameter<float> resX2;
		ofParameter<float> resY2;

		ofParameter<float> posterW2;
		ofParameter<float> posterH2;
		ofParameter<float> rZ2;
		ofParameter<int> alphaVal;
		
		ofParameter<int> backColorR;
		ofParameter<int> backColorG;
		ofParameter<int> backColorB;

		ofxFloatField ppiInput;
		
		int sw, sh;

		ofTrueTypeFont font;
		ReflectionPlane rPlane;
		ReflectionPlane rPlane2;

		ScaleLine line;

		ofxThreadedImage imageSaver;
		ofxTileSaver imageSaveXL;
		bool bSaveImage = false;
		bool bHideGui = false;
	//ofParameter<int> cylRadius;

	public:
		void setup();
		void setupGui();
	void saveImage();
	void onRz(float& value);
		void onResXChange(float& value);
		void onRadiusChange(float& value);
		void onRz2(float& value);
		void onResXChange2(float& value);
		void exportMesh(ofMesh &mesh);
		void update();
		void draw();
		void setNormals(ofMesh &mesh);
		void processOpenedFile(ofFileDialogResult result);
	
		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		void exit();
		
};
